let tooglePersonalizar = document.getElementsByClassName('toogle-personalizar')[0];

let personalizar = document.getElementsByClassName('personalizar')[0];

let tamanhoFont = document.getElementsByClassName('tamanho-font');

let corFundoBranco = document.getElementsByClassName('branco')[0];
let corFundoAmarelo = document.getElementsByClassName('amarelo')[0];
let corFundoVinho = document.getElementsByClassName('vinho')[0];
let corFundoEscuro = document.getElementsByClassName('escuro')[0];

let mudaCapitulo = document.getElementsByClassName('muda-capitulo');

let largura = document.getElementsByClassName('largura');

let espacamento = document.getElementsByClassName('espacamento');

let restaurar = document.getElementsByClassName('restaurar')[0];

let main = document.getElementsByTagName("MAIN")[0];

let p = document.getElementsByTagName("P");

main.style.fontSize = "17px";

function diminuiFont() {
    let tamFontAtual = parseInt(main.style.fontSize.split('px'));
    if (tamFontAtual >= 10) {
        main.style.fontSize = tamFontAtual - 1;
        console.log(main.style.fontSize);
    } else {
        main.style.fontSize = 10;
    }
}

function aumentaFont() {
    tamFontAtual = parseInt(main.style.fontSize.split('px'));
    if (tamFontAtual <= 30) {
        main.style.fontSize = tamFontAtual + 1;
        console.log(main.style.fontSize);
    } else {
        main.style.fontSize = 30;
    }
}

tamanhoFont[0].addEventListener("click", function(e) {
    e.preventDefault();
    diminuiFont();
});

tamanhoFont[1].addEventListener("click", function(e) {
    e.preventDefault();
    aumentaFont();
});

function mudaFundo(corFundo, corLetra) {
    document.body.style.backgroundColor = corFundo;
    document.body.style.color = corLetra;
    mudaCapitulo[0].style.borderColor = corLetra;
    mudaCapitulo[0].style.color = corLetra;
    mudaCapitulo[1].style.borderColor = corLetra;
    mudaCapitulo[1].style.color = corLetra;
    mudaCapitulo[2].style.borderColor = corLetra;
    mudaCapitulo[2].style.color = corLetra;

}
function mudaCorFont(cor) {
    tooglePersonalizar.style.color = cor;
    tamanhoFont[0].style.color = cor;
    tamanhoFont[1].style.color = cor;
    largura[0].style.color = cor;
    largura[1].style.color = cor;
    espacamento[0].style.color = cor;
    espacamento[1].style.color = cor;
}
corFundoBranco.addEventListener("click", function(e) {
    e.preventDefault();
    mudaFundo("#ffffff", "#313131");
    mudaCorFont("#313131");
});

corFundoAmarelo.addEventListener("click", function(e) {
    e.preventDefault();
    mudaFundo("#fdf6e4", "#313131");
    mudaCorFont("#313131");
});

corFundoVinho.addEventListener("click", function(e) {
    e.preventDefault();
    mudaFundo("#800000", "#ffffff");
    mudaCorFont("#ffffff");
})

corFundoEscuro.addEventListener("click", function(e) {
    e.preventDefault();
    mudaFundo("#313131", "#ffffff");
    mudaCorFont("#ffffff");
});

// Muda a largura
main.style.width = "1000px";

function diminuiLargura() {
    larguraAtual = parseInt(main.style.width.split('px'));
    if (larguraAtual > 600) {
        main.style.width = larguraAtual - 50;
        console.log(main.style.width);
    } else {
        console.log("Tamanho mínimo");
    }
}

function aumentaLargura() {
    larguraAtual = parseInt(main.style.width.split('px'));
    if (larguraAtual < screen.availWidth - 150){
        main.style.width = larguraAtual + 50;
        console.log(main.style.width);
    } else {
        console.log("Tamanho máximo");
    }
}

largura[0].addEventListener("click", function(e) {
    e.preventDefault();
    diminuiLargura();
});

largura[1].addEventListener("click", function(e) {
    e.preventDefault();
    aumentaLargura();
});

for(i = 0; i < p.length; i++) {
    p[i].style.lineHeight = "200%";
}

function diminuiEspacamento() {
    for(i = 0; i < p.length; i++) {
        espacamentoAtual = parseInt(p[i].style.lineHeight.split('%'));
        if (espacamentoAtual > 150) {
            p[i].style.lineHeight = espacamentoAtual - 10 + '%';
            console.log(p[i].style.lineHeight);
        }
    }
}

function aumentaEspacamento() {
    for(i = 0; i < p.length; i++) {
        espacamentoAtual = parseInt(p[i].style.lineHeight.split('%'));
        if (espacamentoAtual < 250) {
            p[i].style.lineHeight = espacamentoAtual + 10 + '%';
            console.log(p[i].style.lineHeight);
        }
    }
}

espacamento[0].addEventListener("click", function(e) {
    e.preventDefault();
    diminuiEspacamento();
});

espacamento[1].addEventListener("click", function(e) {
    e.preventDefault();
    aumentaEspacamento();
});

restaurar.addEventListener("click", function(e) {
    main.style.width = "1000px";
    main.style.fontSize = "17px";
    for(i = 0; i < p.length; i++) {
        p[i].style.lineHeight = "200%";
    }
});
personalizar.style.display = 'none';
tooglePersonalizar.addEventListener("click", function(e) {
    if (personalizar.style.display == 'inline-block') {
        personalizar.style.display = 'none';
    } else if (personalizar.style.display == 'none') {
        personalizar.style.display = 'inline-block';
    }
});








